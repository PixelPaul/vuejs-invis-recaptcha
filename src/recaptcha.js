export default {
	name: 'InvisibleRecaptcha',
	props: {
		sitekey: {
			type: String,
			required: true
		},
		badge: {
			type: String,
			required: false
		},
		placement: {
			type: String,
			required: false
		},
		theme: {
			type: String,
			required: false
		},
		validate: {
			type: Function,
			required: false
		},
		onVerify: {
			type: Function,
			required: true
		},
		onExpired: {
			type: Function,
			required: false
		},
		autoReset:{
			type: Boolean,
			required: false,
			default: true
		}
	},
	data: function() {
		return {
			widgetId: false,
			loaded: false,
			container: false
		}
	},
	methods: {
		render: function() {
			if(this.placement && document.getElementById(this.placement)){
				this.container = document.createElement('div')
				document.getElementById(this.placement).insertBefore(this.container,document.getElementById(this.placement).firstChild)
			}else{
				this.container = this.$slots.default ? this.$el.children[0] : this.$el
			}
			this.widgetId = window.grecaptcha.render(this.container, {
				sitekey: this.sitekey,
				size: 'invisible',
				badge: this.badge || 'bottomright',
				theme: this.theme || 'light',
				callback: token => {
					this.onVerify(token)
					if(this.autoReset){
						this.reset()
					}
				},
				'expired-callback': () => {
					if(typeof this.onExpired === 'undefined' && this.autoReset){
						this.reset()
					}else if(typeof this.onExpired === 'function'){
						this.onExpired()
					}
				}
			})
			this.loaded = true
			this.$emit('loaded')
		},
		renderWait: function() {
			setTimeout(() => {
				if (typeof window.grecaptcha !== 'undefined' && window.grecaptcha.render) this.render()
				else this.renderWait()
			}, 200)
		},
		reset: function() {
			window.grecaptcha.reset(this.widgetId)
		},
		execute: function() {
			const cb = (valid) => {
				if(valid){
					if(this.widgetId!==false)
					window.grecaptcha.execute(this.widgetId)
				}
			}
			if(typeof this.validate !== 'function' || this.validate(cb)){
				if(this.widgetId!==false)
				window.grecaptcha.execute(this.widgetId)
			}
		}
	},
	mounted: function() {
		if (typeof window.grecaptcha === 'undefined') {
			var script = document.createElement('script')
			script.src = 'https://www.google.com/recaptcha/api.js?render=explicit'
			script.onload = this.renderWait
			document.head.appendChild(script)
		} else this.render()
	},
	destroyed: function() {
		if(this.placement && this.container)
		document.getElementById(this.placement).removeChild(this.container)
	},
	render (h) {
    return h('div', {}, this.$slots.default)
  }
}
